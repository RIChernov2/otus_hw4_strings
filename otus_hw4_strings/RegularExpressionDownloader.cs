﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_hw4_strings
{
    internal class RegularExpressionDownloader
    {
        private readonly PageWorker _pageWorker;
        private readonly Parser _parser;
        private readonly ImageSaver _imageSaver;
        private string _pageContent;

        public RegularExpressionDownloader()
        {
            _pageWorker = new PageWorker();
            _parser = new Parser();
            _imageSaver = new ImageSaver();
        }

        public bool CheckUrl(string url) => _pageWorker.CheckUrl(url);

        public async Task GetImageFromPageAsync(string url)
        {
            _pageContent = GetContentOrEmpty(url);
            if ( _pageContent == "" ) return;

            List<(string,string)> imagesData = _parser.GetImageUrl(_pageContent);

            foreach ( var imageData in imagesData )
            {
                await _imageSaver.SaveImageAsync(imageData, new Uri(url));
            }

            Console.WriteLine($"Изображения сохранены тут:{Environment.NewLine}\"{_imageSaver._folderPath}\"");
            return;
        }

        private string GetContentOrEmpty(string url)
        {
            string falseMessage = $"Ошибка при вводе url.{Environment.NewLine}";

            string pageContent = "";
            try
            {
                pageContent = _pageWorker.GetPageContent(url);
            }
            catch
            {
                Console.WriteLine(falseMessage);
            }
            return pageContent;
        }
    }
}
