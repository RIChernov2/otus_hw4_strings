﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace otus_hw4_strings
{
    internal class Parser
    {
        private static readonly string groupName = "srcBody";
        private static readonly string _namePattern = @"[^\/]+(png|jpg|jpeg)$";
        private static readonly string _srcPattern = @$"<\s?img.*(src=[""'`](?<{groupName}>.*?)[""'`])";
        private Regex _nameRegex = new Regex(_namePattern);
        private Regex _srcRegex = new Regex(_srcPattern);

        public List<(string srcPart, string name)> GetImageUrl(string pageUrl)
        {   
            return _srcRegex.Matches(pageUrl)
                    .Select(e => e.Groups[groupName].Value)
                    .Select(e => (e, _nameRegex.Match(e).Value))
                    .Distinct()
                    .ToList();
        }
    }
}


// что надо искать
//<img src="/hello.png">
//<img src="https://hello.png">
//<img src='/hello.png' >
//<img src='https://hello.png'>
//<img src="/hello.jpg">
//<img src="https://hello.jpg">
//<img src='/hello.jpg'>
//<img src='https://hello.jpg'>


