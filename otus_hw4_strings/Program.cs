﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;

namespace otus_hw4_strings
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // на этотм сайте тестировал
            //string testUrl = "https://wallpapercave.com/star-trek-wallpaper-1920x1080";

            string url = "";
            bool urlCorrect = false;
            RegularExpressionDownloader downloader = new RegularExpressionDownloader();
            do
            {

                Console.WriteLine("Введите URL для скачивания картинок:");
                url = Console.ReadLine();

                urlCorrect = downloader.CheckUrl(url);
                if ( !urlCorrect )
                {
                    Console.WriteLine("Введен некорректный URL!" + Environment.NewLine);
                    continue;
                }

                await downloader.GetImageFromPageAsync(url);
            }
            while ( !urlCorrect );

            Console.WriteLine("Операция завершена");
            Console.ReadLine();
        }        
    }
}
