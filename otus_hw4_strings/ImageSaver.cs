﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace otus_hw4_strings
{
    internal class ImageSaver
    {
        private readonly WebClient _webClient;
        internal readonly string _folderPath;
        private int _counter;

        public ImageSaver()
        {
            _webClient = new WebClient();
            _folderPath = Path.Combine(Environment.CurrentDirectory, "DownloadedImages");
            CreateDirectoryIfNeededDeleteFiles();
            _counter = 1;
        }

        public async Task SaveImageAsync((string srcPart, string name) imageData, Uri siteUri)
        {
            // отсекаем потенциальные ошибки ошибки
            if ( imageData.name == "" ) return;

            string url;
            if ( imageData.srcPart.StartsWith("/") )
            {
                url = new Uri(siteUri, imageData.srcPart).AbsoluteUri;
            }
            else url = imageData.srcPart;

            await SaveImageViaUrlAsync(url, imageData.name);
        }
        private async Task SaveImageViaUrlAsync(string imageUrl, string fileName)
        {
            Uri uri = new Uri(imageUrl);
            await _webClient.DownloadFileTaskAsync(uri, _folderPath + "\\" + fileName);

            Console.WriteLine($"{_counter++} сохренен файл => {_folderPath}\\{fileName}");
            // почему Async метод возвращает void?
            //_webClient.DownloadFileAsync(uri, _folderPath + "\\" + fileName);
        }
        private void CreateDirectoryIfNeededDeleteFiles()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(_folderPath);
            if ( !directoryInfo.Exists )
            {
                directoryInfo.Create();
            }

            foreach ( FileInfo file in directoryInfo.EnumerateFiles() )
            {
                file.Delete();
            }
        }
    }
}
