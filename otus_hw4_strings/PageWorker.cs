﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace otus_hw4_strings
{
    internal class PageWorker
    {
        public string GetPageContent(string url)
        {
            using ( WebClient client = new WebClient() )
            {
               return client.DownloadString(url);
            }
        }
        public bool CheckUrl(string url)
        {
            bool urlCorrect = false;
            try
            {
                if ( url != "" )
                {
                    WebRequest request = WebRequest.Create(url);
                    using ( WebResponse response = request.GetResponse() )
                    {
                        if ( response != null )
                        {
                            urlCorrect = true;
                        }
                    }                        
                }
            }
            catch { }
            return urlCorrect;
        }
    }
}
